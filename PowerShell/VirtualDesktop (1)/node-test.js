const {spawn, exec} = require('child_process')

// const psPin = exec('powershell.exe', '. ./test.ps1')
// const psPin = spawn('powershell.exe', ['ls'])
const psPin = spawn('powershell.exe', ['.', './test.ps1', ';', 'Lock-AppToWindow', 'notepad'])

 

// console.log(psPin)

psPin.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
  });
  
  psPin.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
  });
  
  psPin.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
  });
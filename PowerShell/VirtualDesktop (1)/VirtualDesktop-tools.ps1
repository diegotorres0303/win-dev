. .\VirtualDesktop.ps1

# Pin-Window ((Get-Process "slack").MainWindowHandle)

# this get the property names for an item
# $items[0].MainWindowHandle | Get-Member | Format-Table



function Lock-AppToWindow ($AppName ) {
    Write-Host "locking $AppName to Virtual Desk"
    $items = (Get-Process $AppName)
    
    ForEach ($item in $items) {
        $items[0].MainWindowHandle | Get-Member | Format-Table
        IF ($item.MainWindowHandle.ToInt32() -gt 100) {
            # $item.MainWindowHandle
            Pin-Window ($item.MainWindowHaZndle)
        }
    }
}

function Unlock-AppToWindow ($AppName ) {
    Write-Host "unlocking $AppName to Virtual Desk"
    $items = (Get-Process $AppName)
    
    ForEach ($item in $items) {
        IF ($item.MainWindowHandle.ToInt32() -gt 100) {
            write-host $AppName "=>" $item.MainWindowHandle
            Unpin-Window ($item.MainWindowHandle)
        }
    }
}





function Set-CursonPosition ($X, $Y) {
    
    # $Cursor = [system.windows.forms.cursor]::Clip
    # [system.windows.forms.cursor]::Position | Get-Member
    [system.windows.forms.cursor]::Position = New-Object system.drawing.point($X, $Y)
}

# [System.Guid]::NewGuid()

function Set-WorkingMode($client) {
    [System.Environment]::SetEnvironmentVariable(
        'working', $client,
        [System.EnvironmentVariableTarget]::User
    )

}


function Send-keys($string) {
    IF ($string -like 'apps') {
        [System.Windows.Forms.SendKeys]::SendWait("^%{TAB}{LEFT}")
        return 
    }
    [System.Windows.Forms.SendKeys]::SendWait($string)
}

function Add-Activity() {
    param(
        [Parameter(Mandatory=$true)][string] $ActivityName,
        [ValidateSet("start", "stop", "end")][string] $Status,
        [string] $Client,
        [string] $Project
    )

    $date = Get-Date -Format g
    Write-Output $date, $ActivityName,  $Status, $Client, $Project
    Add-Content -Path .\res.txt -Value  "$date, $ActivityName,  $Status, $Client, $Project"

}